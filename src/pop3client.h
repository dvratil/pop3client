#pragma once

#include <QObject>
#include <QMap>
#include <QList>
#include <QSet>

#include <KMime/Message>

#include "pop3protocol.h"
#include "jobs.h"
#include "settings.h"

class POP3Client : public QObject
{
    Q_OBJECT
public:
    enum ClientStatus {
        ClientIdle,
        Running,
        Broken,
    };
    Q_ENUM(ClientStatus)

    explicit POP3Client(QObject *parent = nullptr);
    ~POP3Client() override;

    void startSync();

Q_SIGNALS:
    void status(POP3Client::ClientStatus status, const QString &message);

private Q_SLOTS:
    // For state Precommand
    void precommandResult(KJob *job);

    // For state RequestPassword
    void walletOpenedForLoading(QKeychain::Job *baseJob);

    // For state Login
    void loginJobResult(KJob *job);

    // For state List
    void listJobResult(KJob *job);

    // For state UIDList
    void uidListJobResult(KJob *job);

    // For state Download
    void messageFinished(int messageId, KMime::Message::Ptr message);
    void fetchJobResult(KJob *job);
    void messageDownloadProgress(KJob *job, KJob::Unit unit, qulonglong totalBytes);

    // For state Delete
    void deleteJobResult(KJob *job);

    // For state Quit
    void quitJobResult(KJob *job);

private:
    enum State {
        Idle,
        FetchTargetCollection,
        Precommand,
        RequestPassword,
        Connect,
        Login,
        List,
        UIDList,
        Download,
        Save,
        Quit,
        SavePassword,
        CheckRemovingMessage
    };

    void resetState();
    void doStateStep();
    void advanceState(State nextState);
    void cancelSync(const QString &errorMessage, bool error = true);
    void saveSeenUIDList();
    QList<int> shouldDeleteId(int downloadedId) const;
    int idToTime(int id) const;
    int idOfOldestMessage(const QSet<int> &idList) const;
    void startMailCheck();
    void showPasswordDialog(const QString &queryText);
    QString buildLabelForPasswordDialog(const QString &detailedError) const;
    void checkRemovingMessageFromServer();
    void finish();

    void clearCachedPassword();

    bool shouldAdvanceToQuitState() const;

    State mState;
    POPSession *mPopSession = nullptr;
    bool mAskAgain = false;
    QString mPassword;
    bool mSavePassword = false;
    bool mTestLocalInbox = false;

    Settings mSettings;

    // Maps IDs on the server to message sizes on the server
    QMap<int, int> mIdsToSizeMap;

    // Maps IDs on the server to UIDs on the server.
    // This can be empty, if the server doesn't support UIDL
    QMap<int, QString> mIdsToUidsMap;

    // Maps UIDs on the server to IDs on the server.
    // This can be empty, if the server doesn't support UIDL
    QMap<QString, int> mUidsToIdsMap;

    // Whether we actually received a valid UID list from the server
    bool mUidListValid;

    // IDs of messages that we have successfully downloaded. This does _not_ mean
    // that the messages corresponding to the IDs are stored in Akonadi yet
    QList<int> mDownloadedIDs;

    // IDs of messages that we want to download and that we have started the
    // FetchJob with. After the FetchJob, this should be empty, except if there
    // was some error
    QList<int> mIdsToDownload;

    // List of message IDs that were successfully stored in Akonadi
    QList<int> mIDsStored;

    // List of message IDs that were successfully deleted
    QList<int> mDeletedIDs;

    // List of message IDs that we want to delete with the next delete job
    QList<int> mIdsWaitingToDelete;

    // List of message IDs that we want to keep on the server
    mutable QSet<int> mIdsToSave;
    mutable bool mIdsToSaveValid;

    // Current deletion job in process
    DeleteJob *mDeleteJob = nullptr;
};
#include "mainwindow.h"

#include <KMessageBox>
#include <KLocalizedString>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , mSettings(KSharedConfig::openConfig())
{
    ui.setupUi(this);

    loadConfig();

    connect(ui.syncButton, &QPushButton::clicked, this, &MainWindow::sync);
}

MainWindow::~MainWindow()
{
}

void MainWindow::sync()
{
    saveConfig();
}

void MainWindow::loadConfig()
{
    mSettings.load();

    ui.hostEdit->setText(mSettings.host());
    ui.portEdit->setValue(mSettings.port());
    ui.usernameEdit->setText(mSettings.login());
    if (mSettings.useTLS()) {
        ui.encryptionCombo->setCurrentText(QStringLiteral("TLS"));
    } else if (mSettings.useSSL()) {
        ui.encryptionCombo->setCurrentText(QStringLiteral("SSL"));
    } else {
        ui.encryptionCombo->setCurrentText(QStringLiteral("None"));
    }

    if (mSettings.authenticationMethod() == MailTransport::Transport::EnumAuthenticationType::PLAIN) {
        ui.authCombo->setCurrentText(QStringLiteral("PLAIN"));
    } else if (mSettings.authenticationMethod() == MailTransport::Transport::EnumAuthenticationType::LOGIN) {
        ui.authCombo->setCurrentText(QStringLiteral("LOGIN"));
    } else {
        KMessageBox::error(this, i18n("Unknown authentication method"),
                           i18n("Configuration Error"));
    }
}

void MainWindow::saveConfig()
{
    mSettings.setHost(ui.hostEdit->text());
    mSettings.setPort(ui.portEdit->value());
    mSettings.setLogin(ui.usernameEdit->text());
    if (const auto method = ui.encryptionCombo->currentText(); method == QLatin1String("TLS")) {
        mSettings.setUseTLS(true);
        mSettings.setUseSSL(false);
    } else if (method == QLatin1String("SSL")) {
        mSettings.setUseTLS(false);
        mSettings.setUseSSL(true);
    } else {
        mSettings.setUseTLS(false);
        mSettings.setUseSSL(false);
    }

    if (const auto method = ui.authCombo->currentText(); method == QLatin1String("PLAIN")) {
        mSettings.setAuthenticationMethod(MailTransport::Transport::EnumAuthenticationType::PLAIN);
    } else if (method == QLatin1String("LOGIN")) {
        mSettings.setAuthenticationMethod(MailTransport::Transport::EnumAuthenticationType::LOGIN);
    } else {
        KMessageBox::error(this, i18n("Unknown authentication method"),
                           i18n("Configuration Error"));
    }

    mSettings.save();
}
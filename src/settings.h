#pragma once

#include <KSharedConfig>

#include <MailTransport/Transport>

class Settings
{
public:
    explicit Settings(KSharedConfig::Ptr config);

    void load();
    void save();

    QString host() const;
    void setHost(const QString &host);

    uint16_t port() const;
    void setPort(uint16_t port);

    QString login() const;
    void setLogin(const QString &login);

    MailTransport::Transport::EnumAuthenticationType authenticationMethod() const;
    void setAuthenticationMethod(MailTransport::Transport::EnumAuthenticationType authenticationMethod);

    bool useProxy() const;
    void setUseProxy(bool useProxy);

    bool useSSL() const;
    void setUseSSL(bool useSSL);

    bool useTLS() const;
    void setUseTLS(bool useTLS);

    bool pipelining() const;
    void setPipeline(bool pipelining);

    bool leaveOnServer() const;
    void setLeaveOnServer(bool leaveOnServer);

    int leaveOnServerDays() const;
    void setLeaveOnServerDays(int leaveOnServerDays);

    int leaveOnServerCount() const;
    void setLeaveOnServerCount(int leaveOnServerCount);

    int leaveOnServerSize() const;
    void setLeaveOnServerSize(int leaveOnServerSize);

    QList<int> seenUidTimeList() const;
    void setSeenUidTimeList(const QList<int> &seenUidTimeList);

    QStringList seenUidList() const;
    void setSeenUidList(const QStringList &seenUidList);

    QString precommand() const;
private:
    KSharedConfig::Ptr mSettings;
};
#pragma once

#include <QMainWindow>

#include "ui_mainwindow.h"
#include "settings.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private Q_SLOTS:
    void sync();

private:
    void loadConfig();
    void saveConfig();

    Ui::MainWindow ui;
    Settings mSettings;
};
include(ECMQtDeclareLoggingCategory)

add_executable(pop3client)

ecm_qt_declare_logging_category(pop3_common_SRCS HEADER pop3protocol_debug.h IDENTIFIER POP3_LOG CATEGORY_NAME org.kde.pim.pop3protocol
    DESCRIPTION "pop3 protocol (kdepim-runtime)"
    EXPORT KDEPIMRUNTIME
    )

target_sources(pop3client
    PRIVATE
        main.cpp
        mainwindow.ui
        mainwindow.cpp
        pop3client.cpp
        pop3protocol.cpp
        jobs.cpp
        settings.cpp
        ${pop3_common_SRCS}
)

target_include_directories(pop3client
    PRIVATE
    ${CMAKE_CURRENT_BINARY_DIR}
)

target_link_libraries(pop3client
    PRIVATE
    Qt6::Core
    qt6keychain
    KF6::KIOCore
    KF6::KIOWidgets
    KF6::ConfigCore
    KF6::I18n

    KPim6::Mime
    KPim6::MailTransport

    Sasl2::Sasl2
)

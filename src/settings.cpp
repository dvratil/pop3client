#include "settings.h"

Settings::Settings(KSharedConfig::Ptr config)
    : mSettings(config)
{}

void Settings::load()
{
    mSettings->sync();
}

void Settings::save()
{
    mSettings->sync();
}

QString Settings::host() const
{
    return mSettings->group("General").readEntry("host");
}

void Settings::setHost(const QString &host)
{
    mSettings->group("General").writeEntry("host", host);
}

uint16_t Settings::port() const
{
    return static_cast<uint16_t>(
        mSettings->group("General").readEntry<int>("port", 110));
}

void Settings::setPort(uint16_t port)
{
    mSettings->group("General").writeEntry<int>("port", port);
}

QString Settings::login() const
{
    return mSettings->group("General").readEntry("login");
}

void Settings::setLogin(const QString &login)
{
    mSettings->group("General").writeEntry("login", login);
}

MailTransport::Transport::EnumAuthenticationType Settings::authenticationMethod() const
{
    return static_cast<MailTransport::Transport::EnumAuthenticationType>(
        mSettings->group("General").readEntry("authenticationMethod",
            static_cast<int>(MailTransport::Transport::EnumAuthenticationType::LOGIN)));
}

void Settings::setAuthenticationMethod(MailTransport::Transport::EnumAuthenticationType authenticationMethod)
{
    mSettings->group("General").writeEntry("authenticationMethod", static_cast<int>(authenticationMethod));
}

bool Settings::useProxy() const
{
    return mSettings->group("General").readEntry("useProxy", false);
}

void Settings::setUseProxy(bool useProxy)
{
    mSettings->group("General").writeEntry("useProxy", useProxy);
}

bool Settings::useSSL() const
{
    return mSettings->group("General").readEntry("useSSL", false);
}

void Settings::setUseSSL(bool useSSL)
{
    mSettings->group("General").writeEntry("useSSL", useSSL);
}

bool Settings::useTLS() const
{
    return mSettings->group("General").readEntry("useTLS", false);
}

void Settings::setUseTLS(bool useTLS)
{
    mSettings->group("General").writeEntry("useTLS", useTLS);
}

bool Settings::pipelining() const
{
    return mSettings->group("General").readEntry("pipelining", false);
}

void Settings::setPipeline(bool pipelining)
{
    mSettings->group("General").writeEntry("pipelining", pipelining);
}

bool Settings::leaveOnServer() const
{
    return mSettings->group("General").readEntry("leaveOnServer", false);
}

void Settings::setLeaveOnServer(bool leaveOnServer)
{
    mSettings->group("General").writeEntry("leaveOnServer", leaveOnServer);
}

int Settings::leaveOnServerDays() const
{
    return mSettings->group("General").readEntry("leaveOnServerDays", 365);
}

void Settings::setLeaveOnServerDays(int leaveOnServerDays)
{
    mSettings->group("General").writeEntry("leaveOnServerDays", leaveOnServerDays);
}

int Settings::leaveOnServerCount() const
{
    return mSettings->group("General").readEntry("leaveOnServerCount", 100);
}

void Settings::setLeaveOnServerCount(int leaveOnServerCount)
{
    mSettings->group("General").writeEntry("leaveOnServerCount", leaveOnServerCount);
}

int Settings::leaveOnServerSize() const
{
    return mSettings->group("General").readEntry("leaveOnServerSize", 100);
}

void Settings::setLeaveOnServerSize(int leaveOnServerSize)
{
    mSettings->group("General").writeEntry("leaveOnServerSize", leaveOnServerSize);
}

QList<int> Settings::seenUidTimeList() const
{
    return mSettings->group("General").readEntry("seenUidTimeList", QList<int>());
}

void Settings::setSeenUidTimeList(const QList<int> &seenUidTimeList)
{
    mSettings->group("General").writeEntry("seenUidTimeList", seenUidTimeList);
}

QStringList Settings::seenUidList() const
{
    return mSettings->group("General").readEntry("seenUidList", QStringList());
}

void Settings::setSeenUidList(const QStringList &seenUidList)
{
    mSettings->group("General").writeEntry("seenUidList", seenUidList);
}

QString Settings::precommand() const
{
    return {};
}
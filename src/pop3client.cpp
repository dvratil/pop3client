#include "pop3client.h"
#include "jobs.h"
#include "pop3protocol.h"
#include "pop3protocol_debug.h"

#include <MailTransport/PrecommandJob>
#include <MailTransport/Transport>

#include <KAuthorized>
#include <KLocalizedString>
#include <KMessageBox>
#include <KPasswordDialog>

#include <QPointer>
#include <qt6keychain/keychain.h>

using namespace QKeychain;
using namespace MailTransport;

POP3Client::POP3Client(QObject *parent)
    : QObject(parent)
    , mState(Idle)
    , mSettings(KSharedConfig::openConfig())
{
    resetState();
}

POP3Client::~POP3Client()
{
    mSettings.save();
}

void POP3Client::startSync()
{
    Q_ASSERT(mState == Idle);
    startMailCheck();
}

QString POP3Client::buildLabelForPasswordDialog(const QString &detailedError) const
{
    const QString queryText = i18n("Please enter the username and password for.") + QLatin1String("<br>") + detailedError;
    return queryText;
}

void POP3Client::walletOpenedForLoading(QKeychain::Job *baseJob)
{
    auto job = qobject_cast<ReadPasswordJob *>(baseJob);
    bool passwordLoaded = false;
    Q_ASSERT(job);
    if (!job->error()) {
        mPassword = job->textData();
        passwordLoaded = true;
    } else {
        passwordLoaded = false;
        qCWarning(POP3_LOG) << "We have an error during reading password " << job->errorString();
    }
    if (!passwordLoaded) {
        const QString queryText = buildLabelForPasswordDialog(i18n("You are asked here because the password could not be loaded from the wallet."));
        showPasswordDialog(queryText);
    } else {
        advanceState(Connect);
    }
}

void POP3Client::showPasswordDialog(const QString &queryText)
{
    QPointer<KPasswordDialog> dlg = new KPasswordDialog(nullptr, KPasswordDialog::ShowUsernameLine);
    dlg->setRevealPasswordAvailable(KAuthorized::authorize(QStringLiteral("lineedit_reveal_password")));
    dlg->setModal(true);
    dlg->setUsername(mSettings.login());
    dlg->setPassword(mPassword);
    dlg->setPrompt(queryText);

    bool gotIt = false;
    if (dlg->exec()) {
        mPassword = dlg->password();
        mSettings.setLogin(dlg->username());
        mSettings.save();
        if (!dlg->password().isEmpty()) {
            mSavePassword = true;
        }

        mAskAgain = false;
        advanceState(Connect);
        gotIt = true;
    }
    delete dlg;
    if (!gotIt) {
        cancelSync(i18n("No username and password supplied."));
    }
}

void POP3Client::advanceState(State nextState)
{
    mState = nextState;
    doStateStep();
}

void POP3Client::doStateStep()
{
    switch (mState) {
    case Idle:
        Q_ASSERT(false);
        qCWarning(POP3_LOG) << "State machine should not be called in idle state!";
        break;
    case FetchTargetCollection: {
        qCDebug(POP3_LOG) << "================ Starting state FetchTargetCollection ==========";
        Q_EMIT status(Running, i18n("Preparing transmission from."));
        advanceState(Precommand);
        break;
    }
    case Precommand:
        qCDebug(POP3_LOG) << "================ Starting state Precommand =====================";
        if (!mSettings.precommand().isEmpty()) {
            auto precommandJob = new PrecommandJob(mSettings.precommand(), this);
            connect(precommandJob, &PrecommandJob::result, this, &POP3Client::precommandResult);
            precommandJob->start();
            Q_EMIT status(Running, i18n("Executing precommand."));
        } else {
            advanceState(RequestPassword);
        }
        break;
    case RequestPassword: {
        qCDebug(POP3_LOG) << "================ Starting state RequestPassword ================";

        const bool passwordNeeded = mSettings.authenticationMethod() != MailTransport::Transport::EnumAuthenticationType::GSSAPI;
        const bool loadPasswordFromWallet = !mAskAgain && passwordNeeded && !mSettings.login().isEmpty() && mPassword.isEmpty();
        if (loadPasswordFromWallet) {
            auto readJob = new ReadPasswordJob(QStringLiteral("pop3"), this);
            connect(readJob, &QKeychain::Job::finished, this, &POP3Client::walletOpenedForLoading);
            readJob->setKey(mSettings.login());
            readJob->start();
        } else if (passwordNeeded && (mPassword.isEmpty() || mAskAgain)) {
            QString detail;
            if (mAskAgain) {
                detail = i18n("You are asked here because the previous login was not successful.");
            } else if (mSettings.login().isEmpty()) {
                detail = i18n("You are asked here because the username you supplied is empty.");
            }

            showPasswordDialog(buildLabelForPasswordDialog(detail));
        } else {
            // No password needed or using previous password, go on with Connect
            advanceState(Connect);
        }

        break;
    }
    case Connect:
        qCDebug(POP3_LOG) << "================ Starting state Connect ========================";
        Q_ASSERT(!mPopSession);
        mPopSession = new POPSession(mSettings, mPassword);
        advanceState(Login);
        break;
    case Login: {
        qCDebug(POP3_LOG) << "================ Starting state Login ==========================";

        auto loginJob = new LoginJob(mPopSession);
        connect(loginJob, &LoginJob::result, this, &POP3Client::loginJobResult);
        loginJob->start();
        break;
    }
    case List: {
        qCDebug(POP3_LOG) << "================ Starting state List ===========================";
        Q_EMIT status(Running, i18n("Fetching mail listing."));
        auto listJob = new ListJob(mPopSession);
        connect(listJob, &ListJob::result, this, &POP3Client::listJobResult);
        listJob->start();
        break;
    }
    case UIDList: {
        qCDebug(POP3_LOG) << "================ Starting state UIDList ========================";
        auto uidListJob = new UIDListJob(mPopSession);
        connect(uidListJob, &UIDListJob::result, this, &POP3Client::uidListJobResult);
        uidListJob->start();
        break;
    }
    case Download: {
        qCDebug(POP3_LOG) << "================ Starting state Download =======================";

        // Determine which mails we want to download. Those are all mails which are
        // currently on their server, minus the ones we have already downloaded (we
        // remember which UIDs we have downloaded in the settings)
        QList<int> idsToDownload = mIdsToSizeMap.keys();
        const QStringList alreadyDownloadedUIDs = mSettings.seenUidList();
        for (const QString &uidOnServer : std::as_const(mIdsToUidsMap)) {
            if (alreadyDownloadedUIDs.contains(uidOnServer)) {
                const int idOfUIDOnServer = mUidsToIdsMap.value(uidOnServer, -1);
                Q_ASSERT(idOfUIDOnServer != -1);
                idsToDownload.removeAll(idOfUIDOnServer);
            }
        }
        mIdsToDownload = idsToDownload;
        qCDebug(POP3_LOG) << "We are going to download" << mIdsToDownload.size() << "messages";

        // For proper progress, the job needs to know the sizes of the messages, so
        // put them into a list here
        QList<int> sizesOfMessagesToDownload;
        sizesOfMessagesToDownload.reserve(idsToDownload.count());
        for (int id : std::as_const(idsToDownload)) {
            sizesOfMessagesToDownload.append(mIdsToSizeMap.value(id));
        }

        if (mIdsToDownload.empty()) {
            advanceState(CheckRemovingMessage);
        } else {
            auto fetchJob = new FetchJob(mPopSession);
            fetchJob->setFetchIds(idsToDownload, sizesOfMessagesToDownload);
            connect(fetchJob, &FetchJob::result, this, &POP3Client::fetchJobResult);
            connect(fetchJob, &FetchJob::messageFinished, this, &POP3Client::messageFinished);
            connect(fetchJob, &FetchJob::processedAmountChanged, this, &POP3Client::messageDownloadProgress);

            fetchJob->start();
        }
        break;
    }
    case Save:
        qCDebug(POP3_LOG) << "================ Starting state Save ===========================";
        if (shouldAdvanceToQuitState()) {
            advanceState(Quit);
        }
        break;
    case Quit: {
        qCDebug(POP3_LOG) << "================ Starting state Quit ===========================";
        auto quitJob = new QuitJob(mPopSession);
        connect(quitJob, &QuitJob::result, this, &POP3Client::quitJobResult);
        quitJob->start();
        break;
    }
    case SavePassword:
        qCDebug(POP3_LOG) << "================ Starting state SavePassword ===================";
        if (mSavePassword) {
            qCDebug(POP3_LOG) << "Writing password back to the wallet.";
            Q_EMIT status(Running, i18n("Saving password to the wallet."));
            auto writeJob = new WritePasswordJob(QStringLiteral("pop3"), this);
            connect(writeJob, &QKeychain::Job::finished, this, [this](QKeychain::Job *baseJob) {
                if (baseJob->error()) {
                    qCWarning(POP3_LOG) << "Error writing password using QKeychain:" << baseJob->errorString();
                }
                finish();
            });
            writeJob->setKey(mSettings.login());
            writeJob->setTextData(mPassword);
            writeJob->start();
        } else {
            finish();
        }
        break;
    case CheckRemovingMessage:
        qCDebug(POP3_LOG) << "================ Starting state CheckRemovingMessage ===================";
        checkRemovingMessageFromServer();
        break;
    }
}

void POP3Client::checkRemovingMessageFromServer()
{
    const QList<int> idToDeleteMessage = shouldDeleteId(-1);
    if (!idToDeleteMessage.isEmpty()) {
        mIdsWaitingToDelete << idToDeleteMessage;
        if (!mDeleteJob) {
            mDeleteJob = new DeleteJob(mPopSession);
            mDeleteJob->setDeleteIds(mIdsWaitingToDelete);
            mIdsWaitingToDelete.clear();
            connect(mDeleteJob, &DeleteJob::result, this, &POP3Client::deleteJobResult);
            mDeleteJob->start();
        }
    } else {
        advanceState(Save);
    }
}

void POP3Client::precommandResult(KJob *job)
{
    if (job->error()) {
        cancelSync(i18n("Error while executing precommand.") + QLatin1Char('\n') + job->errorString());
        return;
    } else {
        advanceState(RequestPassword);
    }
}

void POP3Client::loginJobResult(KJob *job)
{
    if (job->error()) {
        qCDebug(POP3_LOG) << job->error() << job->errorText();
        if (job->error() == POP3Protocol::ERR_CANNOT_LOGIN) {
            mAskAgain = true;
        }
        cancelSync(i18n("Unable to login to the server \"%1\".", mSettings.host()) + QLatin1Char('\n') + job->errorString());
    } else {
        advanceState(List);
    }
}

void POP3Client::listJobResult(KJob *job)
{
    if (job->error()) {
        cancelSync(i18n("Error while getting the list of messages on the server.") + QLatin1Char('\n') + job->errorString());
    } else {
        auto listJob = qobject_cast<ListJob *>(job);
        Q_ASSERT(listJob);
        mIdsToSizeMap = listJob->idList();
        mIdsToSaveValid = false;
        qCDebug(POP3_LOG) << "IdsToSizeMap size" << mIdsToSizeMap.size();
        advanceState(UIDList);
    }
}

void POP3Client::uidListJobResult(KJob *job)
{
    if (job->error()) {
        cancelSync(i18n("Error while getting list of unique mail identifiers from the server.") + QLatin1Char('\n') + job->errorString());
    } else {
        auto listJob = qobject_cast<UIDListJob *>(job);
        Q_ASSERT(listJob);
        mIdsToUidsMap = listJob->uidList();
        mUidsToIdsMap = listJob->idList();
        qCDebug(POP3_LOG) << "IdsToUidsMap size" << mIdsToUidsMap.size();
        qCDebug(POP3_LOG) << "UidsToIdsMap size" << mUidsToIdsMap.size();

        mUidListValid = !mIdsToUidsMap.isEmpty() || mIdsToSizeMap.isEmpty();
        if (mSettings.leaveOnServer() && !mUidListValid) {
            // FIXME: this needs a proper parent window
            KMessageBox::error(nullptr,
                               i18n("Your POP3 server does not support "
                                    "the UIDL command: this command is required to determine, in a reliable way, "
                                    "which of the mails on the server KMail has already seen before;\n"
                                    "the feature to leave the mails on the server will therefore not "
                                    "work properly."));
        }

        advanceState(Download);
    }
}

void POP3Client::fetchJobResult(KJob *job)
{
    if (job->error()) {
        cancelSync(i18n("Error while fetching mails from the server.") + QLatin1Char('\n') + job->errorString());
        return;
    } else {
        qCDebug(POP3_LOG) << "Downloaded" << mDownloadedIDs.size() << "mails";

        if (!mIdsToDownload.isEmpty()) {
            qCWarning(POP3_LOG) << "We did not download all messages, there are still some remaining "
                                           "IDs, even though we requested their download:"
                                        << mIdsToDownload;
        }

        advanceState(Save);
    }
}

void POP3Client::messageFinished(int messageId, KMime::Message::Ptr message)
{
    if (mState != Download) {
        // This can happen if the slave does not get notified in time about the fact
        // that the job was killed
        return;
    }

    qCDebug(POP3_LOG) << "Got message" << messageId
        << "with subject" << message->subject()->asUnicodeString();

    mDownloadedIDs.append(messageId);
    mIdsToDownload.removeAll(messageId);
}

void POP3Client::messageDownloadProgress(KJob *job, KJob::Unit unit, qulonglong totalBytes)
{
    Q_UNUSED(totalBytes)
    Q_UNUSED(unit)
    Q_ASSERT(unit == KJob::Bytes);
    QString statusMessage;
    const int totalMessages = mIdsToDownload.size() + mDownloadedIDs.size();
    int bytesRemainingOnServer = 0;
    const auto seenUidList{mSettings.seenUidList()};
    for (const QString &alreadyDownloadedUID : seenUidList) {
        const int alreadyDownloadedID = mUidsToIdsMap.value(alreadyDownloadedUID, -1);
        if (alreadyDownloadedID != -1) {
            bytesRemainingOnServer += mIdsToSizeMap.value(alreadyDownloadedID);
        }
    }

    if (mSettings.leaveOnServer() && bytesRemainingOnServer > 0) {
        statusMessage = i18n(
            "Fetching message %1 of %2 (%3 of %4 KB)"
            "(%6 KB remain on the server).",
            mDownloadedIDs.size() + 1,
            totalMessages,
            job->processedAmount(KJob::Bytes) / 1024,
            job->totalAmount(KJob::Bytes) / 1024,
            bytesRemainingOnServer / 1024);
    } else {
        statusMessage = i18n("Fetching message %1 of %2 (%3 of %4 KB)",
                             mDownloadedIDs.size() + 1,
                             totalMessages,
                             job->processedAmount(KJob::Bytes) / 1024,
                             job->totalAmount(KJob::Bytes) / 1024);
    }
    Q_EMIT status(Running, statusMessage);
}

int POP3Client::idToTime(int id) const
{
    const QString uid = mIdsToUidsMap.value(id);
    if (!uid.isEmpty()) {
        const QList<QString> seenUIDs = mSettings.seenUidList();
        const QList<int> timeOfSeenUids = mSettings.seenUidTimeList();
        Q_ASSERT(seenUIDs.size() == timeOfSeenUids.size());
        const int index = seenUIDs.indexOf(uid);
        if (index != -1 && (index < timeOfSeenUids.size())) {
            return timeOfSeenUids.at(index);
        }
    }

    // If we don't find any mail, either we have no UID, or it is not in the seen UID
    // list. In that case, we assume that the mail is new, i.e. from now
    return time(nullptr);
}

int POP3Client::idOfOldestMessage(const QSet<int> &idList) const
{
    int timeOfOldestMessage = time(nullptr) + 999;
    int idOfOldestMessage = -1;
    for (int id : idList) {
        const int idTime = idToTime(id);
        if (idTime < timeOfOldestMessage) {
            timeOfOldestMessage = idTime;
            idOfOldestMessage = id;
        }
    }
    Q_ASSERT(idList.isEmpty() || idOfOldestMessage != -1);
    return idOfOldestMessage;
}

QList<int> POP3Client::shouldDeleteId(int downloadedId) const
{
    QList<int> idsToDeleteFromServer;
    // By default, we delete all messages. But if we have "leave on server"
    // rules, we can save some messages.
    if (mSettings.leaveOnServer()) {
        idsToDeleteFromServer = mIdsToSizeMap.keys();
        if (!mIdsToSaveValid) {
            mIdsToSaveValid = true;
            mIdsToSave.clear();

            const QSet<int> idsOnServer(idsToDeleteFromServer.constBegin(), idsToDeleteFromServer.constEnd());

            // If the time-limited leave rule is checked, add the newer messages to
            // the list of messages to keep
            if (mSettings.leaveOnServerDays() > 0) {
                const int secondsPerDay = 86400;
                const time_t timeLimit = time(nullptr) - (secondsPerDay * mSettings.leaveOnServerDays());
                for (int idToDelete : idsOnServer) {
                    const int msgTime = idToTime(idToDelete);
                    if (msgTime >= timeLimit) {
                        mIdsToSave << idToDelete;
                    } else {
                        qCDebug(POP3_LOG) << "Message" << idToDelete << "is too old and will be deleted.";
                    }
                }
            }
            // Otherwise, add all messages to the list of messages to keep - this may
            // be reduced in the following number-limited leave rule and size-limited
            // leave rule checks
            else {
                mIdsToSave = idsOnServer;
            }

            //
            // Delete more old messages if there are more than mLeaveOnServerCount
            //
            if (mSettings.leaveOnServerCount() > 0) {
                const int numToDelete = mIdsToSave.count() - mSettings.leaveOnServerCount();
                if (numToDelete > 0 && numToDelete < mIdsToSave.count()) {
                    // Get rid of the first numToDelete messages
                    for (int i = 0; i < numToDelete; i++) {
                        mIdsToSave.remove(idOfOldestMessage(mIdsToSave));
                    }
                } else if (numToDelete >= mIdsToSave.count()) {
                    mIdsToSave.clear();
                }
            }

            //
            // Delete more old messages until we're under mLeaveOnServerSize MBs
            //
            if (mSettings.leaveOnServerSize() > 0) {
                const qint64 limitInBytes = mSettings.leaveOnServerSize() * (1024 * 1024);
                qint64 sizeOnServerAfterDeletion = 0;
                for (int id : std::as_const(mIdsToSave)) {
                    sizeOnServerAfterDeletion += mIdsToSizeMap.value(id);
                }
                while (sizeOnServerAfterDeletion > limitInBytes) {
                    const int oldestId = idOfOldestMessage(mIdsToSave);
                    mIdsToSave.remove(oldestId);
                    sizeOnServerAfterDeletion -= mIdsToSizeMap.value(oldestId);
                }
            }
        }
        // Now save the messages from deletion
        //
        for (int idToSave : std::as_const(mIdsToSave)) {
            idsToDeleteFromServer.removeAll(idToSave);
        }
        if (downloadedId != -1 && !mIdsToSave.contains(downloadedId)) {
            idsToDeleteFromServer << downloadedId;
        }
    } else {
        if (downloadedId != -1) {
            idsToDeleteFromServer << downloadedId;
        } else {
            idsToDeleteFromServer << mIdsToSizeMap.keys();
        }
    }
    return idsToDeleteFromServer;
}

void POP3Client::deleteJobResult(KJob *job)
{
    if (job->error()) {
        cancelSync(i18n("Failed to delete the messages from the server.") + QLatin1Char('\n') + job->errorString());
        return;
    }

    auto finishedDeleteJob = qobject_cast<DeleteJob *>(job);
    Q_ASSERT(finishedDeleteJob);
    Q_ASSERT(finishedDeleteJob == mDeleteJob);
    mDeletedIDs = finishedDeleteJob->deletedIDs();

    // Remove all deleted messages from the list of already downloaded messages,
    // as it is no longer necessary to store them (they just waste space)
    QList<QString> seenUIDs = mSettings.seenUidList();
    QList<int> timeOfSeenUids = mSettings.seenUidTimeList();
    Q_ASSERT(seenUIDs.size() == timeOfSeenUids.size());
    for (int deletedId : std::as_const(mDeletedIDs)) {
        const QString deletedUID = mIdsToUidsMap.value(deletedId);
        if (!deletedUID.isEmpty()) {
            int index = seenUIDs.indexOf(deletedUID);
            if (index != -1) {
                // TEST
                qCDebug(POP3_LOG) << "Removing UID" << deletedUID << "from the seen UID list, as it was deleted.";
                seenUIDs.removeAt(index);
                timeOfSeenUids.removeAt(index);
            }
        }
        mIdsToUidsMap.remove(deletedId);
        mIdsToSizeMap.remove(deletedId);
    }
    mSettings.setSeenUidList(seenUIDs);
    mSettings.setSeenUidTimeList(timeOfSeenUids);
    mSettings.save();

    mDeleteJob = nullptr;
    if (!mIdsWaitingToDelete.isEmpty()) {
        mDeleteJob = new DeleteJob(mPopSession);
        mDeleteJob->setDeleteIds(mIdsWaitingToDelete);
        mIdsWaitingToDelete.clear();
        connect(mDeleteJob, &DeleteJob::result, this, &POP3Client::deleteJobResult);
        mDeleteJob->start();
    }

    if (shouldAdvanceToQuitState()) {
        advanceState(Quit);
    } else if (mDeleteJob == nullptr) {
        advanceState(Save);
    }
}

void POP3Client::finish()
{
    qCDebug(POP3_LOG) << "================= Mail check finished. =============================";
    saveSeenUIDList();
    if (mDownloadedIDs.isEmpty()) {
        Q_EMIT status(ClientIdle, i18n("Finished mail check, no message downloaded."));
    } else {
        Q_EMIT status(ClientIdle, i18np("Finished mail check, 1 message downloaded.", "Finished mail check, %1 messages downloaded.", mDownloadedIDs.size()));
    }

    resetState();
}

bool POP3Client::shouldAdvanceToQuitState() const
{
    return mState == Save && mIdsWaitingToDelete.isEmpty() && !mDeleteJob;
}

void POP3Client::quitJobResult(KJob *job)
{
    if (job->error()) {
        cancelSync(i18n("Unable to complete the mail fetch.") + QLatin1Char('\n') + job->errorString());
        return;
    }

    advanceState(SavePassword);
}

void POP3Client::saveSeenUIDList()
{
    QList<QString> seenUIDs = mSettings.seenUidList();
    QList<int> timeOfSeenUIDs = mSettings.seenUidTimeList();

    //
    // Find the messages that we have successfully stored, but did not actually get
    // deleted.
    // Those messages, we have to remember, so we don't download them again.
    //
    QList<int> idsOfMessagesDownloadedButNotDeleted = mIDsStored;
    for (int deletedId : std::as_const(mDeletedIDs)) {
        idsOfMessagesDownloadedButNotDeleted.removeAll(deletedId);
    }
    QList<QString> uidsOfMessagesDownloadedButNotDeleted;
    for (int id : std::as_const(idsOfMessagesDownloadedButNotDeleted)) {
        const QString uid = mIdsToUidsMap.value(id);
        if (!uid.isEmpty()) {
            uidsOfMessagesDownloadedButNotDeleted.append(uid);
        }
    }
    Q_ASSERT(seenUIDs.size() == timeOfSeenUIDs.size());
    for (const QString &uid : std::as_const(uidsOfMessagesDownloadedButNotDeleted)) {
        if (!seenUIDs.contains(uid)) {
            seenUIDs.append(uid);
            timeOfSeenUIDs.append(time(nullptr));
        }
    }

    //
    // If we have a valid UID list from the server, delete those UIDs that are in
    // the seenUidList but are not on the server.
    // This can happen if someone else than this resource deleted the mails from the
    // server which we kept here.
    //
    if (mUidListValid) {
        QList<QString>::iterator uidIt = seenUIDs.begin();
        QList<int>::iterator timeIt = timeOfSeenUIDs.begin();
        while (uidIt != seenUIDs.end()) {
            const QString curSeenUID = *uidIt;
            if (!mUidsToIdsMap.contains(curSeenUID)) {
                // Ok, we have a UID in the seen UID list that is not anymore on the server.
                // Therefore remove it from the seen UID list, it is not needed there anymore,
                // it just wastes space.
                uidIt = seenUIDs.erase(uidIt);
                timeIt = timeOfSeenUIDs.erase(timeIt);
            } else {
                ++uidIt;
                ++timeIt;
            }
        }
    } else {
        qCWarning(POP3_LOG) << "UID list from server is not valid.";
    }

    //
    // Now save it in the settings
    //
    qCDebug(POP3_LOG) << "The seen UID list has" << seenUIDs.size() << "entries";
    mSettings.setSeenUidList(seenUIDs);
    mSettings.setSeenUidTimeList(timeOfSeenUIDs);
    mSettings.save();
}

void POP3Client::cancelSync(const QString &errorMessage, bool error)
{
    if (error) {
        qCWarning(POP3_LOG) << "============== ERROR DURING POP3 SYNC ==========================";
        qCWarning(POP3_LOG) << errorMessage;
    } else {
        qCDebug(POP3_LOG) << "Canceled the sync, but no error.";
    }
    saveSeenUIDList();
    resetState();
}

void POP3Client::resetState()
{
    mState = Idle;
    mIdsToSizeMap.clear();
    mIdsToUidsMap.clear();
    mUidsToIdsMap.clear();
    mDownloadedIDs.clear();
    mIdsToDownload.clear();
    mIDsStored.clear();
    mDeletedIDs.clear();
    mIdsWaitingToDelete.clear();
    if (mDeleteJob) {
        mDeleteJob->deleteLater();
        mDeleteJob = nullptr;
    }
    mUidListValid = false;
    mSavePassword = false;

    if (mPopSession) {
        mPopSession->abortCurrentJob(); // no-op since jobs are sync
        // Disconnect after sending the QUIT command.
        mPopSession->deleteLater();
        mPopSession = nullptr;
    }
}

void POP3Client::startMailCheck()
{
    resetState();
    advanceState(FetchTargetCollection);
}

void POP3Client::clearCachedPassword()
{
    mPassword.clear();
}
